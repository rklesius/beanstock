USE traderdb;

# Insert some values

INSERT INTO stock VALUES (1,'AAPL');
INSERT INTO stock VALUES (2,'GOOG');
INSERT INTO stock VALUES (3,'BRK-A');
INSERT INTO stock VALUES (4,'NSC');
INSERT INTO stock VALUES (5,'MSFT');

INSERT INTO simple_strategy VALUES (2,1,199,1000,25.34,1,75.87,NULL);
INSERT INTO simple_strategy VALUES (3,2,19,1050,45.67,1,56.8,current_date());
INSERT INTO simple_strategy VALUES (4,3,1912,100,4.67,2,5.8,current_date());
INSERT INTO simple_strategy VALUES (5,4,1923,102,45.63,0,53.8,NULL);
