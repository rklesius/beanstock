package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.Trade;

public interface TradeDao {

	/**
	 * Insert the trade if it doesn't exists or
	 * Update the trade if already exists
	 * @param trade
	 * @return id
	 */
    int save(Trade trade);

    /**
     * Find all trade (need to be a trade of an existing Stock)
     * @return list of trades
     */
    List<Trade> findAll();

    /**
     * Find trade by its id
     * @param id
     * @return trade
     */
    Trade findById(int id);

    /**
     * Find all trades with certain state
     * @param state
     * @return
     */
    List<Trade> findAllByState(Trade.TradeState state);

    /**
     * Delete trade by id
     * @param id
     */
    void deleteById(int id);
    
    // by Denis
    Trade findByTicker(String ticker);
}
