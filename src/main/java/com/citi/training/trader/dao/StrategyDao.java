package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.Strategy;

public interface StrategyDao {

    List<Strategy> findAll();
    
    List<Strategy> findTop(int number_of_rows);

    /**
     * Create a strategy or update if already exists.
     * new strategies are create with only Stock and size
     * @param strategy
     * @return
     */
    int save(Strategy strategy);
    
    double getprofits();
}
