package com.citi.training.trader.dao;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

public interface PriceDao {

	/**
	 * Records the new price of the stock
	 * @param price
	 * @return price_id
	 */
    int create(Price price);

    /**
     * Finds all recorded prices of a certain stock. 
     * For example, all prices that AAL has had.
     * @param stock
     */
    List<Price> findAll(Stock stock);
    
    /**
     * Find the last 'count' prices of the stock
     * @param stock
     * @param count
     */
    List<Price> findLatest(Stock stock, int count);

    
    /**
     * Delete all recorder prices that occurred later than the cutOffTime
     * @param cutOffTime
     * @return number of prices deleted
     */
    int deleteOlderThan(LocalDateTime cutOffTime);
    
    /**
     * Find price of the stock before the cutOffTime
     * @param stock
     * @param cutOffTime
     * @return
     */
    List<Price> findBeforeThan(Stock stock, LocalDateTime cutOffTime);
}
