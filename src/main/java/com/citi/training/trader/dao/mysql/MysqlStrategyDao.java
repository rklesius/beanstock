package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.StrategyService;
import com.citi.training.trader.service.StockService;
import com.citi.training.trader.model.Stock;

/**
 * JDBC MySQL DAO implementation for simple_strategy table.
 *
 */
@Component
public class MysqlStrategyDao implements StrategyDao {

    private static final Logger logger =
                            LoggerFactory.getLogger(MysqlStrategyDao.class);

    private static String FIND_ALL_SQL = "select simple_strategy.id as strategy_id, stock.id as stock_id, stock.ticker, " +
                                         "size, exit_profit_loss, current_position, last_trade_price, profit, stopped " +
                                         "from simple_strategy left join stock on stock.id = simple_strategy.stock_id";
   
    private static String FIND_TOP_SQL = "select simple_strategy.id as strategy_id, stock.id as stock_id, stock.ticker, " +
            "size, exit_profit_loss, current_position, last_trade_price, profit, stopped " +
            "from simple_strategy left join stock on stock.id = simple_strategy.stock_id order by simple_strategy.id desc limit ";

    private static String INSERT_SQL = "INSERT INTO simple_strategy (stock_id, size, exit_profit_loss, current_position, " +
                                       "last_trade_price, profit, stopped) " +
                                       "values (:stock_id, :size, :exit_profit_loss, :current_position, "+ 
                                       ":last_trade_price, :profit, :stopped)";

    private static String UPDATE_SQL = "UPDATE simple_strategy set stock_id=:stock_id, size=:size, " +
                                       "exit_profit_loss=:exit_profit_loss, current_position=:current_position, " +
                                       "last_trade_price=:last_trade_price, profit=:profit, stopped=:stopped where id=:id";
   // by Denis
    private static String FIND_STOCK_SQL = "select simple_strategy.id as strategy_id, stock.id as stock_id, stock.ticker, " +
            "size, exit_profit_loss, current_position, last_trade_price, profit, stopped " +
            "from simple_strategy left join stock on stock.id = simple_strategy.stock_id  " +
            "where simple_strategy.stock_id = ?";

    @Autowired
    private JdbcTemplate tpl;
    
    // by Denis
    @Autowired
    private StrategyService StrategyService;
    @Autowired
    private StockService StockService;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<Strategy> findAll(){
        logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
        return tpl.query(FIND_ALL_SQL,
                         new StrategyMapper());
    }
    public List<Strategy> findTop(int number_of_rows){
        logger.debug("findTop SQL: [" + FIND_TOP_SQL + String.valueOf(number_of_rows) + "]");
        List<Strategy> strategy=  tpl.query(FIND_TOP_SQL + String.valueOf(number_of_rows),new StrategyMapper());
        return strategy.stream().limit(number_of_rows).collect(Collectors.toList());
    }

    public int save(Strategy strategy) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        namedParameters.addValue("stock_id", strategy.getStock().getId());
        namedParameters.addValue("size", strategy.getSize());
        namedParameters.addValue("exit_profit_loss", strategy.getExitProfitLoss());
        namedParameters.addValue("current_position", strategy.getCurrentPosition());
        namedParameters.addValue("last_trade_price", strategy.getLastTradePrice());
        namedParameters.addValue("profit", strategy.getProfit());
        namedParameters.addValue("stopped", strategy.getStopped());
        
        // by Denis
        // Make sure the stock isn't being traded by another strategy
//        int stock_id = StockService.findByTicker(strategy.getStock().getTicker()).getId();
        int stock_id = strategy.getStock().getId();

        List<Strategy> Strategies = this.tpl.query(FIND_STOCK_SQL,
                new Object[]{stock_id},
                new StrategyMapper()
        );

        if(Strategies.size() <= 0) {
            logger.debug("Inserting Strateg: " + strategy);

            KeyHolder keyHolder = new GeneratedKeyHolder();
            namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
            strategy.setId(keyHolder.getKey().intValue());
            
        } else {
        	
        	namedParameters.addValue("id", Strategies.get(0).getId());
        	        	
            logger.debug("Updating simpleStrategy: " + strategy);
            
            namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);

        }

        logger.debug("Saved trade: " + strategy);

        return strategy.getId();
    }
    
    // by Denis
    public double getprofits(){
    	List<Strategy> allStrategy = findAll();
    	double profits = 0;
    	for (Strategy strategy: allStrategy) {
    		if(strategy.getStopped()!=null) {
    			// strategy already closed
    			profits += strategy.getProfit();
    		}
    	}
    	return profits;
    }

    /**
     * private internal class to map database rows to SimpleStrategy objects.
     *
     */
    private static final class StrategyMapper implements RowMapper<Strategy> {
        public Strategy mapRow(ResultSet rs, int rowNum) throws SQLException {
            logger.debug("Mapping simple_strategy result set row num [" + rowNum + "], id : [" +
                         rs.getInt("strategy_id") + "]");

            return new Strategy(rs.getInt("strategy_id"),
                             new Stock(rs.getInt("stock_id"),
                                       rs.getString("stock.ticker")),
                             rs.getInt("size"),
                             rs.getDouble("exit_profit_loss"),
                             rs.getInt("current_position"),
                             rs.getDouble("last_trade_price"),
                             rs.getDouble("profit"),
                             (LocalDateTime)rs.getObject("stopped", LocalDateTime.class)
                             );
        }
    }
}
