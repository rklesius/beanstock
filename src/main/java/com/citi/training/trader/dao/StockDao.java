package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.Stock;

public interface StockDao {

    int create(Stock stock);

    /**
     * Find all Stock in stock table
     * @return list of stocks
     */
    List<Stock> findAll();

    /**
     * Find the stock with id='id' 
     * @param id
     * @return a Stock object
     */
    Stock findById(int id);

    /**
     * Find a Stock by its ticker name
     * @param ticker
     * @return a Stock object
     */
    Stock findByTicker(String ticker);

    /**
     * Delete a Stock by its id
     * @param id
     */
    void deleteById(int id);
}
