package com.citi.training.trader.model;

import java.time.LocalDateTime;

public class StrategyMapper {

	private Stock stock;
	private int size;
	private int currentPosition;

	public StrategyMapper(Stock stock, int size, int currentPosition) {
		this.stock = stock;
		this.size = size;
		this.currentPosition = currentPosition;
	}

	public Strategy buildStrategy(StrategyMapper sm) {

		return new Strategy(sm.getStock(), sm.getSize());
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}

}
