package com.citi.training.trader.model;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlTransient;

import org.springframework.beans.factory.annotation.Autowired;

import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

public class TradeMapper {

	private double price;
	private int size;
	private Stock stock;

	private TradeType tradeType;

	public TradeMapper(double price, int size, Stock stock, TradeType tradeType) {
		this.price = price;
		this.size = size;
		this.stock = stock;
		this.tradeType = tradeType;
	}

	public Trade buildTrade(TradeMapper tm) {

		Trade trade = new Trade(tm.getStock(), tm.getPrice(), tm.getSize(), 
				tm.getTradeType(), new Strategy(tm.getStock(), tm.getSize()));

		return trade;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public TradeType getTradeType() {
		return tradeType;
	}

	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}

}
