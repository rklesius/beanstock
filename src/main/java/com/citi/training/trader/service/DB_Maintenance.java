package com.citi.training.trader.service;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;

@Component
public class DB_Maintenance {
	
	private static final Logger logger = LoggerFactory.getLogger(PriceService.class);

	@Value("${db.maintenance.max_table_size:1000}")
	int max_table_size;
	
	@Autowired
    private PriceDao priceDao;
	@Autowired
    private StockDao stockDao;
	@Autowired
    private TradeDao tradeDao;
	@Autowired
    private StrategyDao strategyDao;
	
	
	/**
	 * Performs regular maintenance with no user input, the time and table size
	 * can be configured through the application.properties file
	 */
	@Scheduled(fixedRateString = "${db.maintenance.interval_ms:10000}")
	public void maintain() {
		logger.warn("Deleting prices older than "+LocalDateTime.now().minusHours(2));
		priceDao.deleteOlderThan(LocalDateTime.now().minusHours(2));
		
		logger.warn("Deleting REJECTED, PARTIALLY_FILLED and CANCELED trades");
		tradeDao.findAllByState(Trade.TradeState.REJECTED);
		tradeDao.findAllByState(Trade.TradeState.PARTIALLY_FILLED);
		tradeDao.findAllByState(Trade.TradeState.CANCELED);
	}
	
	
}
