package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.strategy.StrategyAlgorithm;
import com.citi.training.trader.strategy.TwoMovingAveragesStrategy;

@Component
public class StrategyService {

	@Autowired
	private StrategyDao strategyDao;

	public List<Strategy> findAll() {
		return strategyDao.findAll();
	}
	
	public List<Strategy> findTop(int number_of_elements) {
		return strategyDao.findTop(number_of_elements);
	}

	/**
	 * Create or update strategy
	 * @param strategy
	 */
	public void save(Strategy strategy) {
		strategyDao.save(strategy);
	}
	
	public double getprofits() {
		return strategyDao.getprofits();
	}
	
	// by Denis
	// Automatically running Strategies
//	@Autowired
//	TwoMovingAveragesStrategy twoMovingAveragesStrategy ;
//	twoMovingAveragesStrategy.run(); 
	
	
}
