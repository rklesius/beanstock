package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.StockService;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

/**
 * REST Controller for {@link com.citi.training.trader.model.Stock} resource.
 *
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("${com.citi.training.trader.rest.stock-base-path:/price}")
public class PriceController {

	private static final Logger logger = LoggerFactory.getLogger(PriceController.class);

	@Autowired
	private PriceService PriceService;
	@Autowired
	private StockService StockService;

	@RequestMapping(value = "/{stock_ticker}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Price> findAll(@PathVariable String stock_ticker) {
		logger.debug("findAll(" + stock_ticker + ")");	
		return PriceService.findAll(StockService.findByTicker(stock_ticker));
	}

	@RequestMapping(value = "/{stock_ticker}/{count}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Price> findLatest(@PathVariable String stock_ticker, @PathVariable int count) {
		Stock stock = StockService.findByTicker(stock_ticker);
		logger.debug("findLatest(" + stock.getTicker() + ", " + count + ")");
		return PriceService.findLatest(stock, count);
	}

}
