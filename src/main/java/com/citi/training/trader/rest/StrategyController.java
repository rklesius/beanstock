package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.service.StrategyService;
import com.citi.training.trader.service.StockService;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.StrategyMapper;
import com.citi.training.trader.model.Trade;

/**
 * REST Controller for {@link com.citi.training.trader.model.Strategy} resource.
 *
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("${com.citi.training.trader.rest.stock-base-path:/strategy}")
public class StrategyController {

	private static final Logger logger = LoggerFactory.getLogger(StrategyController.class);

	@Autowired
	private StrategyService StrategyService;
	@Autowired
	private StockService StockService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Strategy> findAll() {
		logger.debug("findAll()");
		return StrategyService.findAll();
	}

	@RequestMapping(value = "/{number_of_elements}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Strategy> findTop(@PathVariable int number_of_elements) {
		logger.debug("findTop(" + number_of_elements + ")");
		return StrategyService.findTop(number_of_elements);
	}
	
	
	// By denis
	@RequestMapping(method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Strategy> save(@RequestBody StrategyMapper strategyMapper) {
		
		Strategy newstrategy = strategyMapper.buildStrategy(strategyMapper);
		
		int real_stock_id = StockService.findByTicker(newstrategy.getStock().getTicker()).getId();
		
		newstrategy.getStock().setId(real_stock_id);
		
		StrategyService.save(newstrategy);
		
		return new ResponseEntity<Strategy>(newstrategy, HttpStatus.CREATED);

	}
	
	// by Denis
	@RequestMapping(value = "/profits", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public double getprofits() {
		logger.debug("Getting the profits for all close strategies");
		return StrategyService.getprofits();
	}

}







//logger.debug("create(" + strategy + ")");
//Strategy strategy = new Strategy(StockService.findByTicker(stock_ticker),size);
//StrategyService.save(strategy);
//logger.debug("created strategy: " + strategy);
