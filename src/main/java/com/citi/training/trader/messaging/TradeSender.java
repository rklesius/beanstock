package com.citi.training.trader.messaging;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.TradeService;

@Component
public class TradeSender {

    private final static Logger logger =
                    LoggerFactory.getLogger(TradeSender.class);

    @Autowired
    private TradeService tradeService;

    @Autowired
    private JmsTemplate jmstemplate;
    
    @Value("${jms.sender.responseQueueName:OrderBroker_Reply}")
    private String responseQueue;
//
//    /**
//     * Keeps executing every 5 seconds.
//     * It finds all Trades with 'INIT' state and update them to WAITING_FOR_REPLY status
//     */
//    @Scheduled(fixedRateString = "${messaging.tradesender.database_poll_ms:5000}")
//    public void getAndSendTrades() {
//        for(Trade trade: tradeService.findAllByState(Trade.TradeState.INIT)) {
//            sendTrade(trade);
//            trade.stateChange(Trade.TradeState.WAITING_FOR_REPLY);
//            tradeService.save(trade);
//        }
//    }
    
    /**
     * Change the state of the trade to WAITING_FOR_REPLY. 
     * It saves the trade in the database and sends the trade to the Broker 
     * @param tradeToSend
     */

    public void sendTrade(Trade tradeToSend) {
        logger.debug("Sending Trade " + tradeToSend);
        logger.debug("Trade XML:");
        logger.debug(Trade.toXml(tradeToSend));
        
        tradeToSend.stateChange(Trade.TradeState.WAITING_FOR_REPLY);
        tradeService.save(tradeToSend);

        jmstemplate.convertAndSend("OrderBroker", Trade.toXml(tradeToSend), message -> {
            message.setStringProperty("Operation", "update");
            message.setJMSReplyTo(buildReplyTo());
            return message;
        });
    }
    
    private Destination buildReplyTo() throws JMSException {
        final Session session = jmstemplate.getConnectionFactory().createConnection()
            .createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Destination queue =
        		jmstemplate.getDestinationResolver().resolveDestinationName(session, responseQueue, false);
        return queue;
      }

}
