package com.citi.training.trader.strategy;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;

@Component
public class TwoMovingAveragesStrategy implements StrategyAlgorithm {

    private static final Logger logger =
                LoggerFactory.getLogger(SimpleStrategyAlgorithm.class);

    @Autowired
    private PriceDao priceDao;

    @Autowired
    private TradeSender tradeSender;

    @Autowired
    private StrategyDao strategyDao;
    
    @Value("${twomovingaverages.strategy.long.average:50}")
    int longAverageRange;
    @Value("${twomovingaverages.strategy.short.average:5}")
    int shortAverageRange;

    double longAverage=0;
	double shortAverage=0;
	

    /**
     * It goes through all open Strategies and 
     */

	@Scheduled(fixedRateString = "${twomovingaverages.strategy.refresh.rate_ms:5000}")
    public void run() {
    	
    	logger.info("TwoMovingAveragesStrategy run() method is running");

        for(Strategy strategy: strategyDao.findAll()) {
            if(strategy.getStopped() != null) {
                continue;
            }

            if(!strategy.hasPosition()) {
                // we have no open position

                // get latest two prices for this stock
                List<Price> prices = priceDao.findLatest(strategy.getStock(), 2);

                if(prices.size() < 2) {
                    logger.warn("Unable to execute strategy, not enough price data: " +
                                strategy); 
                    continue;
                }

                logger.debug("Taking position based on prices:");
                
                longAverage = getLongAverage(strategy.getStock(), longAverageRange);
    			shortAverage = getShortAverage(strategy.getStock(), shortAverageRange);
    			    			
    			if(longAverage==shortAverage) {
					// don't take a position yet, wait.
					logger.warn("Unable to take a position, not enough price change: " + strategy);
					continue;
				}
				if(longAverage>shortAverage) {
					// buy now, since it will eventually increase
					logger.debug("Taking long position for strategy: " + strategy);
					strategy.takeLongPosition();
					strategy.setLastTradePrice(makeTrade(strategy, Trade.TradeType.BUY));
				}
				else {
					// sell now
					logger.debug("Taking short position for strategy: " + strategy);
					strategy.takeShortPosition();
					strategy.setLastTradePrice(makeTrade(strategy, Trade.TradeType.SELL));
				}

            } else if(strategy.hasLongPosition()) {
                // we have a long position on this stock
                // close the position by selling
                logger.debug("Closing long position for strategy: " + strategy);

                double thisTradePrice = makeTrade(strategy, Trade.TradeType.SELL);
                logger.debug("Bought at: " + strategy.getLastTradePrice() + ", sold at: " +
                             thisTradePrice);
                closePosition(thisTradePrice - strategy.getLastTradePrice(), strategy);

            } else if(strategy.hasShortPosition()) {
                // we have a short position on this stock
                // close the position by buying
                logger.debug("Closing short position for strategy: " + strategy);

                double thisTradePrice = makeTrade(strategy, Trade.TradeType.BUY);
                logger.debug("Sold at: " + strategy.getLastTradePrice() + ", bought at: " +
                             thisTradePrice);

                closePosition(strategy.getLastTradePrice() - thisTradePrice, strategy);
            }
            strategyDao.save(strategy);
            
            // by Denis
            // Decide on whether to close
//            decideIfClose(strategy);
        }
    }
	
	// by Denis
	public void decideIfClose(Strategy strategy) {
		double profit = strategy.getExitProfitLoss();
	}

   /**
    * Change position to closed.
    * Possible positions are (long, short, closed)
    * @param profitLoss
    * @param strategy
    */
    private void closePosition(double profitLoss, Strategy strategy) {
        logger.debug("Recording profit/loss of: " + profitLoss +
                     " for strategy: " + strategy);
        strategy.addProfitLoss(profitLoss);
        strategy.closePosition();
    }


	/**
	 * Sends the Trade to the queue
	 * 
	 * @param strategy
	 * @param tradeType
	 * @return current price of the stock
	 */
	private double makeTrade(Strategy strategy, Trade.TradeType tradeType) {
		List<Price> prices = priceDao.findLatest(strategy.getStock(), 1);
		// by Denis
		// This gave me some problems, if stock is not available in the price table, it will cause problems
		if(prices.size()<=0) {
			logger.debug("no price data for " + strategy.getStock().getTicker());
			return -1;
		}
		else {
			Price currentPrice = prices.get(0);
			tradeSender.sendTrade(
					new Trade(strategy.getStock(), currentPrice.getPrice(), strategy.getSize(), tradeType, strategy));
			return currentPrice.getPrice();
		}

	}
	
	// Create by Denis
	private double getLongAverage(Stock stock,int count) {

		List<Price> priceList = priceDao.findLatest(stock, count);

		// find the total
		double total = 0;
		for (Price price : priceList) {
			total += price.getPrice();
		}
		// return average
		return (double) total / priceList.size();
	}

	// Create by Denis
	private double getShortAverage(Stock stock, int count) {

		List<Price> priceList = priceDao.findLatest(stock, count);

		// find the total
		double total = 0;
		for (Price price : priceList) {
			total += price.getPrice();
		}
		// return average
		return (double) total / priceList.size();
	}
	
	

}
