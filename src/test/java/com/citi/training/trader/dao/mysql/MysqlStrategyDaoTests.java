package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlStrategyDaoTests {

	@Autowired
	MysqlStrategyDao mysqlStrategyDao;
	
	@Autowired
	MysqlStockDao mysqlStockDao;

	// Mock object
	Stock mock_stock = new Stock(2, "GOOG");
	int mock_size = 10;
	Strategy mock_strategy = new Strategy(mock_stock, mock_size);

	@Test
	public void findAll_test() {
		mysqlStrategyDao.findAll();
	}

	@Test
	public void findTop_test() {
		mysqlStrategyDao.findTop(1);
	}

	@Test
	public void save_test() {
		mock_stock.setId(mysqlStockDao.create(mock_stock));
		mysqlStrategyDao.save(mock_strategy);
	}

	@Test
	public void getprofits_test() {
		mysqlStrategyDao.getprofits();
	}
}
