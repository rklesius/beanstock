package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlPriceDaoTests {

	@Autowired
	MysqlPriceDao mysqlPriceDao;

	@Autowired
	MysqlStockDao mysqlStockDao;

	// Mock object
	Stock mock_Stock = new Stock(-1, "GOOG");
	double mock_price = 99.99;
	Price mock_price_obj = new Price(mock_Stock, mock_price);
	LocalDateTime mock_LocalDateTime = LocalDateTime.now();
	LocalDateTime mock_old_LocalDateTime = mock_LocalDateTime.withYear(mock_LocalDateTime.getYear() - 5);

	@Test
	public void create_test() {
		mock_Stock.setId(mysqlStockDao.create(mock_Stock));
		mysqlPriceDao.create(mock_price_obj);
		assert (mysqlPriceDao.findAll(mock_Stock).size() > 0);
//		mysqlPriceDao.findLatest(mock_Stock, 1);

	}

	@Test
	public void deleteOlderThan_test() {

		mysqlPriceDao.deleteOlderThan(mock_old_LocalDateTime);
	}

	@Test
	public void findBeforeThan_test() {

		mysqlPriceDao.findBeforeThan(mock_Stock, mock_old_LocalDateTime);
	}
}
