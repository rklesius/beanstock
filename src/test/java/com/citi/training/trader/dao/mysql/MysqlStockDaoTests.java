package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlStockDaoTests {
	
	@Autowired
	MysqlStockDao mysqlStockDao;
	
	// Create mock object date
	int mock_id = 1001;
	String mock_ticker = "TICK";
	Stock mock_stock = new Stock(mock_id,mock_ticker);
	
	// Notes
	// When inserting a Stock into the table, it will disregard the Stock id
	// and it will create it's own id based on id already in the db. That way
	// there are no conflicts
	
	@Test
	public void findAll_test(){
		
		mysqlStockDao.create(mock_stock);
		List <Stock> query_result = mysqlStockDao.findAll();

		assertEquals(query_result.get(0).getClass(),Stock.class);
		assert(query_result.size()>0);
    }
	@Test
	public void findBy_test(){

		mysqlStockDao.create(mock_stock);
		Stock stock_result = mysqlStockDao.findByTicker(mock_ticker);
		mysqlStockDao.findById(stock_result.getId());
    }

	@Test
	public void deleteById_test(){
		mysqlStockDao.create(mock_stock);
		Stock stock_result = mysqlStockDao.findByTicker(mock_ticker);
		mysqlStockDao.deleteById(stock_result.getId());
    }

}
