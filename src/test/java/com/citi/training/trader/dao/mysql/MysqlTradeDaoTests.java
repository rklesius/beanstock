package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlTradeDaoTests {

	@Autowired
	MysqlTradeDao mysqlTradeDao;

	// Mock object
	String mock_ticker = "GOOG";
	int mock_id = 1;
	Trade mock_trade = new Trade();

	@Test
	public void findAll_test() {
		mysqlTradeDao.findAll();
	}

//	@Test
//	public void findByTicker_test() {
//		mysqlTradeDao.findByTicker(mock_ticker);
//	}

//	@Test
//	public void findById_test() {
//
//		mysqlTradeDao.findById(mock_id);
//	}

	@Test
	public void findAllByState_test() {

		mysqlTradeDao.findAllByState(Trade.TradeState.INIT);
	}

//	@Test
//	public void deleteById_test() {
//
//		mysqlTradeDao.findById(mock_id);
//	}

//	@Test
//	public void save_test() {
//
//		mysqlTradeDao.save(mock_trade);
//	}

}
