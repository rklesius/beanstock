package com.citi.training.trader;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class SpringAutoTraderApplicationTests {

	@Test
	public void contextLoads() {
	}

}
