package com.citi.training.trader.model;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;

public class StrategyTests {

	// Create a mock objects to use throughout the tests
	int mock_id = 1;
	Stock mock_stock = new Stock(2,"tempTicker");
	int mock_size =50;
	double mock_exitProfitLoss = 50.55;
	int mock_currentPosition = 1;
	double mock_lastTradePrice = 108.99;
	double mock_profit = 55.45;
	LocalDateTime mock_stopped = LocalDateTime.now();
	private static final int DEFAULT_MAX_TRADES = 20;

	@Test
    public void test_stock_size_Constructor() {
		int unusedVariable = 0;
	}
		/*Strategy testStrategy = new Strategy(mock_stock,mock_size);
		assertEquals(testStrategy.getId(),-1);
        assertEquals(testStrategy.getStock(), mock_stock);
        assertEquals(testStrategy.getSize(), mock_size);
        assert(testStrategy.getExitProfitLoss()== DEFAULT_MAX_TRADES);
        assertEquals(testStrategy.getCurrentPosition(), 0);
        assert(testStrategy.getLastTradePrice()==0);
        assert(testStrategy.getProfit()==0);
        assertNull(testStrategy.getStopped());
    }*/
	@Test
    public void test_stock_full_Constructor() {
		Strategy testStrategy = 
				new Strategy(mock_id, mock_stock,mock_size, 
						mock_exitProfitLoss,mock_currentPosition,
						mock_lastTradePrice, mock_profit,mock_stopped);
		assertEquals(testStrategy.getId(),mock_id);
        assertEquals(testStrategy.getStock(), mock_stock);
        assertEquals(testStrategy.getSize(), mock_size);
        assert(testStrategy.getExitProfitLoss()== mock_exitProfitLoss);
        assertEquals(testStrategy.getCurrentPosition(), mock_currentPosition);
        assert(testStrategy.getLastTradePrice()==mock_lastTradePrice);
        assert(testStrategy.getProfit()==mock_profit);
        assertEquals(testStrategy.getStopped(),mock_stopped);
    }
	@Test
	public void test_set_methods() {
		Strategy testStrategy = new Strategy(mock_stock,mock_size);
		testStrategy.setCurrentPosition(mock_currentPosition);
		assertEquals(testStrategy.getCurrentPosition(), mock_currentPosition);
		
		testStrategy.setExitProfitLoss(mock_exitProfitLoss);
		assert(testStrategy.getExitProfitLoss()== mock_exitProfitLoss);
		
		testStrategy.setId(mock_id);
		assertEquals(testStrategy.getId(), mock_id);
		
		testStrategy.setLastTradePrice(mock_lastTradePrice);
		assert(testStrategy.getLastTradePrice()== mock_lastTradePrice);
		
		testStrategy.setProfit(mock_profit);
		assert(testStrategy.getProfit()== mock_profit);
		
		testStrategy.setSize(mock_size);
		assertEquals(testStrategy.getSize(), mock_size);
		
		testStrategy.setStock(mock_stock);
		assertEquals(testStrategy.getStock(), mock_stock);
		
		testStrategy.setStopped(mock_stopped);
		assertEquals(testStrategy.getStopped(), mock_stopped);
		
		testStrategy.takeLongPosition();
		assertEquals(testStrategy.getCurrentPosition(),1);
		
		testStrategy.takeShortPosition();
		assertEquals(testStrategy.getCurrentPosition(),-1 );
		
		testStrategy.closePosition();
		assertEquals(testStrategy.getCurrentPosition(),0 );
		
		testStrategy.stop();
		
	}
}
