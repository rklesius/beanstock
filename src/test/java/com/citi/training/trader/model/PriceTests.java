package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;

import org.junit.Test;

public class PriceTests {
	
	// Price class has 3 constructor
	// Create a mock objects to use throughout the tests
	int mock_id = 1;
	Stock mock_stock = new Stock(5,"tempTicker");
	double mock_price = 9.99;
	LocalDateTime mock_time = LocalDateTime.now();

	@Test
    public void test_stock_price_Constructor() {
		Price testPrice = new Price(mock_stock, mock_price);
		
        assert(testPrice.getId() == -1);
        assert(testPrice.getStock().getId() == mock_stock.getId());
        assertEquals(testPrice.getStock().getTicker(),mock_stock.getTicker() );
        assert(testPrice.getPrice() == mock_price);
        assertNotNull(testPrice.getRecordedAt());
    }
	
	@Test
    public void test_stock_price_time_Constructor() {
		Price testPrice = new Price(mock_stock, mock_price, mock_time);
		
        assert(testPrice.getId() == -1);
        assert(testPrice.getStock().getId() == mock_stock.getId());
        assertEquals(testPrice.getStock().getTicker(),mock_stock.getTicker() );
        assert(testPrice.getPrice() == mock_price);
        assertEquals(testPrice.getRecordedAt(),mock_time);
    }
	
	@Test
    public void test_full_Constructor() {
		Price testPrice = new Price(mock_id, mock_stock, mock_price, mock_time);
		
        assert(testPrice.getId() == mock_id);
        assertEquals(testPrice.getStock().getId(), mock_stock.getId());
        assertEquals(testPrice.getStock().getTicker(),mock_stock.getTicker() );
        assert(testPrice.getPrice() == mock_price);
        assertEquals(testPrice.getRecordedAt(),mock_time);
    }
	
	@Test
	public void test_setId() {
		Price testPrice = new Price(mock_id, mock_stock, mock_price, mock_time);
		int temp_id = 99;
		testPrice.setId(temp_id);
		assertEquals(testPrice.getId(),temp_id);
	
	}
	
	@Test
	public void test_setStock() {
		Price testPrice = new Price(mock_id, mock_stock, mock_price, mock_time);
		Stock temp_Stock = new Stock(1,"tempStock");
		testPrice.setStock(temp_Stock);
		assertEquals(testPrice.getStock(),temp_Stock);
	
	}
	
	@Test
	public void test_setPrice() {
		Price testPrice = new Price(mock_id, mock_stock, mock_price, mock_time);
		double temp_price = 99.99;
		testPrice.setPrice(temp_price);
		assert(testPrice.getPrice()==temp_price);
	}
	
	@Test
	public void test_setRecordedAt() {
		Price testPrice = new Price(mock_id, mock_stock, mock_price, mock_time);
		LocalDateTime  temp_RecordedAt = LocalDateTime .now();
		testPrice.setRecordedAt(temp_RecordedAt);
		assert(testPrice.getRecordedAt()==temp_RecordedAt);
	}
	
	@Test
	public void test_toString() {
		Price testPrice = new Price(mock_id, mock_stock, mock_price, mock_time);
		String testStock_to_string = testPrice.toString();
		assertEquals(testStock_to_string, "Price [id=" + testPrice.getId() + ", stock=" + testPrice.getStock() +
				", price=" + testPrice.getPrice() + ", recordedAt=" + testPrice.getRecordedAt() + "]");
	}
	
}
