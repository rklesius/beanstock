package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.citi.training.trader.model.Trade.TradeType;

public class TradeMapperTests {

	// Create mock objects
	 double mock_price = 9.99;
	 int mock_size = 100;
	 Stock mock_stock = new Stock(1,"temp_ticker");
	 TradeType mock_tradeType = Trade.TradeType.BUY;

	 @Test
	 public void test_TradeMapper_constructor() {
		 
		 TradeMapper temp_TradeMapper = new TradeMapper(mock_price,mock_size,mock_stock,mock_tradeType);
		 assert(temp_TradeMapper.getPrice()==mock_price);
		 assertEquals(temp_TradeMapper.getSize(), mock_size);
		 assertEquals(temp_TradeMapper.getStock(), mock_stock);
		 assertEquals(temp_TradeMapper.getTradeType(), mock_tradeType);
	 }
	 
	 @Test
	 public void test_buildTrade() {
		 
		 TradeMapper temp_TradeMapper = new TradeMapper(mock_price,mock_size,mock_stock,mock_tradeType);
		 Trade temp_trade = temp_TradeMapper.buildTrade(temp_TradeMapper);
		 
		 assertEquals(temp_trade.getStock(), mock_stock);
		 assert(temp_trade.getPrice()== mock_price);
		 assertEquals(temp_trade.getSize(), mock_size);
		 assertEquals(temp_trade.getTradeType(), mock_tradeType);
		 
	 }
	 
	 @Test
	 public void test_set_methods() {
		 TradeMapper temp_TradeMapper = new TradeMapper(mock_price,mock_size,mock_stock,mock_tradeType);
		 
		 temp_TradeMapper.setPrice(mock_price);
		 assert(temp_TradeMapper.getPrice()==mock_price);

		 temp_TradeMapper.setSize(mock_size);
		 assert(temp_TradeMapper.getSize()==mock_size);

		 temp_TradeMapper.setStock(mock_stock);
		 assertEquals(temp_TradeMapper.getStock(),mock_stock);

		 temp_TradeMapper.setTradeType(mock_tradeType);
		 assertEquals(temp_TradeMapper.getTradeType(),mock_tradeType);
	 }
}
