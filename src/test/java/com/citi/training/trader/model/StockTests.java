package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.time.LocalDateTime;
import org.junit.Test;

public class StockTests {

	// Create a mock objects to use throughout the tests
	int mock_id = 5;
	String mock_ticker = "tempTicker";

	@Test
	public void test_Stock_Constructor() {
		Stock testStock = new Stock(mock_id, mock_ticker);
		assertEquals(testStock.getId(), mock_id);
		assertEquals(testStock.getTicker(), mock_ticker);

	}

	@Test
	public void test_toString() {
		Stock testStock = new Stock(mock_id, mock_ticker);
		String testStock_to_string = testStock.toString();
		assertEquals(testStock_to_string, "Stock [id=" + testStock.getId() + ", ticker=" + testStock.getTicker() + "]");

	}
	
	@Test
	public void test_setId() {
		Stock testStock = new Stock(mock_id, mock_ticker);
		int temp_id = 99;
		testStock.setId(temp_id);
		assertEquals(testStock.getId(), temp_id);
	}
	@Test
	public void test_setTicker() {
		Stock testStock = new Stock(mock_id, mock_ticker);
		String temp_ticker = "ticker_test";
		testStock.setTicker(temp_ticker);
		assertEquals(testStock.getTicker(),temp_ticker);
	}

}
