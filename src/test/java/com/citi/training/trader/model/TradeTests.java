package com.citi.training.trader.model;

import org.junit.Test;

import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;

import javax.xml.bind.JAXBException;

public class TradeTests {
	
	// Create a mock objects to use throughout the tests
	int mock_id = 1;
	Stock mock_stock = new Stock(5,"tempTicker");
	double mock_price = 9.99;
	int mock_size = 50;
	TradeType mock_tradeType = Trade.TradeType.BUY;
	TradeState mock_tradeState = Trade.TradeState.INIT;
	LocalDateTime mock_lastStateChange = LocalDateTime.now(); 
	Strategy mock_strategy = new Strategy(mock_stock,mock_size);
	
	// String types
	String mock_string_tradeType =  "BUY";		// BUY
	String mock_string_tradeState = "INIT";		// INIT
	
	Trade test_trade = new Trade(mock_id, mock_stock, mock_price, mock_size, mock_tradeType,
			mock_tradeState, mock_lastStateChange, mock_strategy);

	
	@Test
    public void test_stock_price_size_Constructor() {
		
		Trade test_trade = new Trade(mock_stock, mock_price, mock_size, mock_tradeType,mock_strategy);
        
        assert(test_trade.getId()== -1);
        assertEquals(test_trade.getStock(),mock_stock );
        assert(test_trade.getPrice()==mock_price);
        assertEquals(test_trade.getSize() ,mock_size);
        assertEquals(test_trade.getTradeType() ,mock_tradeType);
        assertEquals(test_trade.getState() , Trade.TradeState.INIT);
        assertNotNull(test_trade.getLastStateChange());
        assertEquals(test_trade.getStrategy() ,mock_strategy);
    }
	
	 @Test
	    public void test_string_parameteres_Constructor() {
			
			Trade test_trade = new Trade(mock_id, mock_stock, mock_price, mock_size, mock_string_tradeType,
					mock_string_tradeState, mock_lastStateChange, mock_strategy);
	        
	        assert(test_trade.getId()== mock_id );
	        assertEquals(test_trade.getStock(),mock_stock );
	        assert(test_trade.getPrice()==mock_price);
	        assertEquals(test_trade.getSize() ,mock_size);
	        assertEquals(test_trade.getTradeType() ,Trade.TradeType.BUY);
	        assertEquals(test_trade.getState() , Trade.TradeState.INIT);
	        assertEquals(test_trade.getLastStateChange() ,mock_lastStateChange);
	        assertEquals(test_trade.getStrategy() ,mock_strategy);
	    }
	 

	@Test
    public void test_full_Constructor() {
		
		Trade test_trade = new Trade(mock_id, mock_stock, mock_price, mock_size, mock_tradeType,
				mock_tradeState, mock_lastStateChange, mock_strategy);
        
        assert(test_trade.getId()== mock_id );
        assertEquals(test_trade.getStock(),mock_stock );
        assert(test_trade.getPrice()==mock_price);
        assertEquals(test_trade.getSize() ,mock_size);
        assertEquals(test_trade.getTradeType() ,mock_tradeType);
        assertEquals(test_trade.getState() ,mock_tradeState);
        assertEquals(test_trade.getLastStateChange() ,mock_lastStateChange);
        assertEquals(test_trade.getStrategy() ,mock_strategy);
    }
	
	@Test
	public void test_toString() {
		
		Trade test_trade = new Trade(mock_id, mock_stock, mock_price, mock_size, mock_tradeType,
				mock_tradeState, mock_lastStateChange, mock_strategy);
				
		String expectedString =  		
	               "Trade [id=" + test_trade.getId() + ", stock=" + test_trade.getStock() + 
	               ", tradeType=" + test_trade.getTradeType() +
	               ", price=" + test_trade.getPrice() + ", size=" + test_trade.getSize() +
	               ", state=" + test_trade.getState() + ", lastStateChange=" + test_trade.getLastStateChange() +
	               ", strategyId="+-1;
		
		assertEquals(test_trade.toString(),expectedString);
	}
	
	@Test
	public void test_equal(){
		Trade test_trade = new Trade(mock_id, mock_stock, mock_price, mock_size, mock_tradeType,
				mock_tradeState, mock_lastStateChange, mock_strategy);
		Trade test_trade2 = new Trade(mock_id, mock_stock, mock_price, mock_size, mock_tradeType,
				mock_tradeState, mock_lastStateChange, mock_strategy);
		assert(test_trade.equals(test_trade));
		test_trade2.setId(5);
		assert(test_trade.equals(test_trade));
	}
	
	@Test
	public void test_set_methods(){
		Trade test_trade = new Trade();
		test_trade.setId(1);
		test_trade.setPrice(9.99);
		test_trade.setSize(10);
		test_trade.setState(Trade.TradeState.INIT);
		test_trade.setStock(mock_stock );
		test_trade.setStrategy(new Strategy(mock_stock, mock_size));
		test_trade.setStockTicker("mock_tiker");
	}
	
	@Test
	public void test_get_methods(){
		Trade test_trade = new Trade();
		test_trade.getTempStockTicker();
	}
	
	@Test
	public void test_toXml_no_exception(){
		
		test_trade.toXml(test_trade);
	}

}
