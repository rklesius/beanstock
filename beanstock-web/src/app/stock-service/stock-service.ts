import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export class Stock{
    constructor(
        public id: number,
        public ticker: string
        ) {}
}

@Injectable()
export class StockService {

    private stockUrl: string;

    constructor(private httpClient:HttpClient) { 
        this.stockUrl = environment.rest_host + '/stock';
    }
 
    getStocks(): Observable<Stock[]> {
        return this.httpClient.get<Stock[]>(this.stockUrl); 
    }
}