import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { OpenStrategiesComponent } from './open-strategies/open-strategies.component';
import { ClosedStrategiesComponent } from './closed-strategies/closed-strategies.component';
import { OpenStratDetailsComponent } from './open-strat-details/open-strat-details.component';


const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'open-strategies', component: OpenStrategiesComponent},
  {path: 'closed-strategies', component: ClosedStrategiesComponent},
  {path: 'open-strategies/:stock', component: OpenStratDetailsComponent},
  {path: '**', redirectTo: ''} //wildcard error, go home
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
