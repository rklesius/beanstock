import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Stock } from '../stock-service/stock-service';
import { environment } from '../../environments/environment';

export class Strategy{
    constructor( 
        public id: number,
        public stock: Stock,
        public size: number,
        public exitProfitLoss: number,
        public currentPosition: number,
        public lastTradePrice: number,
        public profit: number,
        public stopped: Date
        ) {}
}

@Injectable()
export class StrategyService {

    private strategyUrl: string;

    constructor(private httpClient:HttpClient) {
        this.strategyUrl = environment.rest_host + '/strategy';
     }
 
    getStrategies(): Observable<Strategy[]> {
        return this.httpClient.get<Strategy[]>(this.strategyUrl);
    }

    createStrategy(strategy:Strategy): Observable<any> {
        return this.httpClient.post<Strategy>(
            this.strategyUrl , strategy
        );
    }

    getStrategiesLimit(limit : number): Observable<Strategy[]> {
        return this.httpClient.get<Strategy[]>(this.strategyUrl + '/' + limit);
    }

    getTotalProfits(): Observable<number> {
        return this.httpClient.get<number>(this.strategyUrl + '/profits');
    }
}