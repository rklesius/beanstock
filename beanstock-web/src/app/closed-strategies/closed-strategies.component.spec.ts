import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosedStrategiesComponent } from './closed-strategies.component';

describe('ClosedStrategiesComponent', () => {
  let component: ClosedStrategiesComponent;
  let fixture: ComponentFixture<ClosedStrategiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosedStrategiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosedStrategiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
