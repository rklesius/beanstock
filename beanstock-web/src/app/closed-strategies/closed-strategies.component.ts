import { Component, OnInit } from '@angular/core';
import { Strategy, StrategyService } from '../strategy-service/strategy-service';
import { Pipe, PipeTransform } from '@angular/core';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-closed-strategies',
  templateUrl: './closed-strategies.component.html',
  styleUrls: ['./closed-strategies.component.css']
})
export class ClosedStrategiesComponent implements OnInit {
  strategies: Strategy[] = [];
  updateSubscription: Subscription;

  constructor(private strategyService : StrategyService) { 
    
  }

  ngOnInit() {
    this.updateStrategies();
  }

  private updateStrategies() {
    this.strategyService.getStrategies().subscribe(response => {
      this.strategies = response.reverse();
      this.startRefreshTimer();
    });
  }

  private startRefreshTimer() {
    this.updateSubscription = timer(10000).subscribe(
      (val) => { this.updateStrategies() }
    )
  }

  ngOnDestroy() {
    this.updateSubscription.unsubscribe();
  }

  public strategiesIsEmpty() : boolean {
    if (this.strategies.length == 0) {
      return true;
    }
    else {
      return false;
    }
  }

  public noClosedStrategies() : boolean {
    for (let strategy of this.strategies) {
      if (strategy.stopped != null) {
        return false;
      }
    }
    return true;  
  }
}


