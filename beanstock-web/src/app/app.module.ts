import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ReactiveFormsModule } from '@angular/forms';

//Components
import { HomepageComponent } from './homepage/homepage.component';
import { OpenStrategiesComponent, PositionString } from './open-strategies/open-strategies.component';
import { OpenStratDetailsComponent } from './open-strat-details/open-strat-details.component';
import { ClosedStrategiesComponent } from './closed-strategies/closed-strategies.component';
import { HeaderComponent } from './header/header.component';


//Services
import { StockService } from './stock-service/stock-service';
import { StrategyService } from './strategy-service/strategy-service';
import { StockChartComponent } from './stock-chart/stock-chart.component';
import { PriceService } from './price-service/price-service';
import { HomepageChartComponent } from './homepage-chart/homepage-chart.component';
import { ProfitsComponent } from './profits/profits.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    OpenStrategiesComponent,
    OpenStratDetailsComponent,
    ClosedStrategiesComponent,
    HeaderComponent,
    PositionString,
    StockChartComponent,
    HomepageChartComponent,
    ProfitsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CollapseModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [PositionString],
  providers: [StockService, StrategyService, PriceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
