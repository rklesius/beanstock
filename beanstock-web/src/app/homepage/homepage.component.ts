import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import { Stock, StockService } from '../stock-service/stock-service';
import { Strategy, StrategyService } from '../strategy-service/strategy-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit 
{
  stocks: Stock[] = [];
  errorMsg : string;

  uploadForm: FormGroup;
  constructor(private stockService: StockService, 
              private strategyService: StrategyService, 
              private formBuilder:FormBuilder, 
              private httpClient:HttpClient,
              private router: Router) 
  { 
    stockService.getStocks().subscribe(response => {
      this.stocks = response;
    });
  }

  stockForm = new FormGroup
  ({
    selectedStock: new FormControl('', Validators.required),
    selectedQuantity: new FormControl('', Validators.required),
    selectedPosition: new FormControl('', Validators.required),
  })

  onSubmit()
  {

    if (this.isValidStock(this.stockForm.value.selectedStock)) {
      let newStrategy: Strategy = this.buildStrategy(this.stockForm.value.selectedStock, 
                        this.stockForm.value.selectedQuantity, 
                        this.stockForm.value.selectedPosition)
      this.strategyService.createStrategy(newStrategy).subscribe();
      this.router.navigate(['/open-strategies']);
    }
    else {
      this.errorMsg = "Invalid stock name";
    }
    if (this.errorMsg) {
      alert(this.errorMsg)
    }
  }
  
  ngOnInit() 
  { }

  private isValidStock(inStock : string) : boolean {
    for(let stock of this.stocks)
    {
      if (stock.ticker === inStock)
      {
        return true;
      }
    }
    return false;
  }

  private buildStrategy(ticker : string, quantity : number, position : string) : Strategy {
    let newStrat = {} as Strategy;
    let newStock = {} as Stock;

    newStock.id = 0;
    newStock.ticker = ticker;

    if (position === "Go Short") {
      newStrat.currentPosition = -1;  //-1 for short
    }
    else {
      newStrat.currentPosition = 1; // 1 for long
    }

    newStrat.stock = newStock;
    newStrat.size = quantity;

    return newStrat;
  }

}
