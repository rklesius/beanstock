import { Component, OnInit, Input, Pipe, PipeTransform } from '@angular/core';
import { Strategy, StrategyService } from '../strategy-service/strategy-service';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-open-strategies',
  templateUrl: './open-strategies.component.html',
  styleUrls: ['./open-strategies.component.css']
})
export class OpenStrategiesComponent implements OnInit {
  strategies: Strategy[] = [];
  updateSubscription: Subscription;
  errorMsg = "Loading...";
  displayBtn = false;

  constructor(private strategyService : StrategyService) { 

  }

  ngOnInit() {
    this.updateStrategies();
  }
  
  private updateStrategies() {
    this.strategyService.getStrategies().subscribe(response => {
      this.strategies = response;
      this.startRefreshTimer();
    });
  }

  private startRefreshTimer() {
    this.updateSubscription = timer(2000).subscribe(
      (val) => { this.updateStrategies() }
    )
  }

  public strategiesIsEmpty() : boolean {
    if (this.strategies.length == 0) {
      this.errorMsg = "No Strategies Created!";
      this.displayBtn = true;
      return true;
    }
    else {
      return false;
    }
  }

  public noOpenStrategies() : boolean {
    for (let strategy of this.strategies) {
      if (strategy.stopped == null) {
        return false;
      }
    }
    this.errorMsg = "No Open Strategies!";
    this.displayBtn = true;
    return true;  
  }
}

@Pipe({name: 'positionString'})
export class PositionString implements PipeTransform {
  transform(value: number): string {
    console.log(value);
    switch(value) {
      case -1: return "Going short...";
      case 1: return "Going long...";
      case 0: return "Updating position...";
      default: return "Position error";
    }
  }
}
