import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenStrategiesComponent } from './open-strategies.component';

describe('OpenStrategiesComponent', () => {
  let component: OpenStrategiesComponent;
  let fixture: ComponentFixture<OpenStrategiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenStrategiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenStrategiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
