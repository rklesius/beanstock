import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenStratDetailsComponent } from './open-strat-details.component';

describe('OpenStratDetailsComponent', () => {
  let component: OpenStratDetailsComponent;
  let fixture: ComponentFixture<OpenStratDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenStratDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenStratDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
