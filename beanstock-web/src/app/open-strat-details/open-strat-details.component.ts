import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Strategy, StrategyService } from '../strategy-service/strategy-service';
import { Subscription, timer } from 'rxjs';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-open-strat-details',
  templateUrl: './open-strat-details.component.html',
  styleUrls: ['./open-strat-details.component.css']
})
export class OpenStratDetailsComponent implements OnInit {
  strategies: Strategy[] = [];
  ticker : string;
  currStrategy = {} as any;
  updateSubscription: Subscription;
  
  constructor(private strategyService : StrategyService, private route: ActivatedRoute) { 
    this.ticker = this.route.snapshot.paramMap.get("stock");
  }

  ngOnInit() {
    this.updateStrategies();
  }

  private updateStrategies() {
    this.strategyService.getStrategies().subscribe(response => {
      this.strategies = response;
      this.getStrategyInfo();
      this.startRefreshTimer();
    });
  }

  private startRefreshTimer() {
    this.updateSubscription = timer(2000).subscribe(
      (val) => { this.updateStrategies() }
    )
  }

  private getStrategyInfo() {
    for (let strategy of this.strategies) {
      if(strategy.stock.ticker == this.ticker) {
        this.currStrategy = strategy;
      }
    }
  }

    

}

@Pipe({name: 'positionString'})
export class PositionString implements PipeTransform {
transform(value: number): string {
  console.log(value);
  switch(value) {
    case -1: return "Going short...";
    case 1: return "Going long...";
    case 0: return "Updating position...";
    default: return "Position error";
  }
}
}