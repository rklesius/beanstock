import { Component, OnInit } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { Chart } from 'chart.js'
import { PriceService, Price } from '../price-service/price-service';
import { StrategyService, Strategy } from '../strategy-service/strategy-service';

@Component({
  selector: 'app-homepage-chart',
  templateUrl: './homepage-chart.component.html',
  styleUrls: ['./homepage-chart.component.css']
})
export class HomepageChartComponent implements OnInit {
  priceLimit: number = 20;
  strategyLimit: number = 3;
  strategies: Strategy[] = [];
  tickers: string[]= []; //Three tickers for top three strategies
  price1: Price[] =[];
  price2: Price[] =[];
  price3: Price[] =[];
  times = [];
  cost1 = []; 
  cost2 = [];
  cost3 = [];
  chart = [];
  chartTitle = "Recent Strategies Stock Prices"

  updateSubscription : Subscription;

  constructor(private strategyService : StrategyService, private priceService: PriceService) { 
    
  }

  ngOnInit() {
    this.updateStrategies();
  }

  //gets the top three tickers and triggers the price data for them
  private updateStrategies() {
    this.strategyService.getStrategiesLimit(this.strategyLimit).subscribe(response => {
      this.strategies = response;
      this.getTickers();
      this.getPrices();
      this.buildGraph();
      this.startRefreshTimer();
    }); 
  }

  private startRefreshTimer() {
    this.updateSubscription = timer(3000).subscribe(
      (val) => { this.updateStrategies() }
    )
  }

  private getTickers() {
    for(let strategy of this.strategies) {
      this.tickers.push(strategy.stock.ticker);
    }
    if(this.tickers.length < 3) {
      this.tickers = ["GOOG", "AAPL", "MSFT"];  //input some sample data if less than three open strategies
      this.chartTitle = "Potential Stocks to Buy: "
    }
  }

  private getPrices() {
    this.priceService.getPricesLimit(this.tickers[0], this.priceLimit).subscribe(response => { 
      this.price1 = response.reverse();
    });
    this.priceService.getPricesLimit(this.tickers[1], this.priceLimit).subscribe(response => { 
      this.price2 = response.reverse();
    });
    this.priceService.getPricesLimit(this.tickers[2], this.priceLimit).subscribe(response => { 
      this.price3 = response.reverse();
    });
  }

  private buildGraph() {
    this.times = [];
    this.cost1 = [];
    this.cost2 = [];
    this.cost3 = []
    for (let price of this.price1) {
      let curDate = new Date(price.recordedAt);
      this.times.push(curDate.toLocaleDateString() + ' ' + curDate.toLocaleTimeString());
      this.cost1.push(price.price);
    }
    for (let price of this.price2) {
      this.cost2.push(price.price);
    }
    for (let price of this.price3) {
      this.cost3.push(price.price);
    }
    this.chart = new  Chart('canvas', {
      type: 'line',
      data: {
        labels: this.times,
        datasets: [
          {
            label: this.tickers[0],
            data: this.cost1,
            borderColor: '#739122',
            fill: false
          },
          {
            label: this.tickers[1],
            data: this.cost2,
            borderColor: '#914022',
            fill: false
          },
          {
            label: this.tickers[2],
            data: this.cost3,
            borderColor: '#227391',
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        animation: {
          duration: 0
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }

}
