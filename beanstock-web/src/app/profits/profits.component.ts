import { Component, OnInit } from '@angular/core';
import { Observable, timer, Subscription } from 'rxjs';
import { StrategyService } from '../strategy-service/strategy-service';

@Component({
  selector: 'app-profits',
  templateUrl: './profits.component.html',
  styleUrls: ['./profits.component.css']
})
export class ProfitsComponent implements OnInit {

  profit: number;
  private priceSubscription: Subscription;

  constructor(private strategyService: StrategyService) { }

  ngOnInit() {
    this.updateProfit();
  }

  private updateProfit() {
    this.strategyService.getTotalProfits().subscribe( data => {
      this.profit = data;
      this.startRefreshTimer();
    });
  }

  private startRefreshTimer() {
    this.priceSubscription = timer(10000).subscribe(
      (val) => { this.updateProfit() }
    );
  }
}
