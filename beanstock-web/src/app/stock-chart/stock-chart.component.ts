import { Component, OnInit, ElementRef } from '@angular/core';
import { PriceService, Price } from '../price-service/price-service';
import { ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-stock-chart',
  templateUrl: './stock-chart.component.html',
  styleUrls: ['./stock-chart.component.css']
})
export class StockChartComponent implements OnInit {
  limit: number = 20;
  ticker: string;
  prices: Price[] = [];
  times = [];
  cost = []; 
  priceChart = [];

  uSubscription: Subscription;

  constructor(private elementRef: ElementRef, private priceService : PriceService, private route: ActivatedRoute) { 
    this.ticker = this.route.snapshot.paramMap.get("stock");

  }

  ngOnInit() {
    this.updatePrices();
  }

  private updatePrices() {
    this.priceService.getPricesLimit(this.ticker, this.limit).subscribe(response => { 
      this.prices = response.reverse();
      this.buildGraph();
      this.startRefreshTimer();
    });
  }

  private startRefreshTimer() {
    this.uSubscription = timer(10000).subscribe(
      (val) => { this.updatePrices() }
    )
  }

  ngOnDestroy() {
    this.uSubscription.unsubscribe();
  }

  private buildGraph() {
    let htmlRef = this.elementRef.nativeElement.querySelector(`#stockChart`)
    this.times = [];
    this.cost = [];
    for (let price of this.prices) {
      let curDate = new Date(price.recordedAt);
      this.times.push(curDate.toLocaleDateString() + ' ' + curDate.toLocaleTimeString());
      this.cost.push(price.price);
    }
    this.priceChart = new Chart(htmlRef, {
      type: 'line',
      data: {
        labels: this.times,
        datasets: [
          {
            data: this.cost,
            borderColor: '#739122',
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        animation: {
          duration: 0
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }



}
