import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Stock } from '../stock-service/stock-service';
import { environment } from '../../environments/environment';

export class Price{
    constructor(
        public id: number,
        public price: number,
        public recordedAt: Date,
        public stock: Stock
        ) {}
}

@Injectable()
export class PriceService {

    private priceUrl: string;

    constructor(private httpClient:HttpClient) { 
        this.priceUrl = environment.rest_host + '/price/';
    }
 
    getPrices(ticker : string): Observable<Price[]> {
        return this.httpClient.get<Price[]>(this.priceUrl + ticker);
    }

    //returns a specific number of prices (count) for the ticker
    getPricesLimit(ticker : string, count : number) : Observable<Price[]> {
        return this.httpClient.get<Price[]>(this.priceUrl + ticker + '/' + count);
    }
}